import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

//Material
import { MaterialModule } from "./app.module.material";

import { Md2Module } from "md2";


//APP
 import { AppRoutingModule } from './app-routing.module';
// import { routing, mainRoutingProviders } from './main.route';

import { RootComponent } from './root.component';
//  import { AppComponent } from './app.component';
import { JuridocService } from './services/juridoc.service';
import { AfsluitenDossierComponent } from './components/afsluiten-dossier/afsluiten-dossier.component';

//shared
import { SharedModule } from './shared/shared.module';
import { HomeComponent } from './components/home/home.component';


@NgModule({
  declarations: [
    RootComponent,
    // AppComponent,
    AfsluitenDossierComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule, BrowserAnimationsModule, FormsModule, ReactiveFormsModule, JsonpModule, HttpModule,
    MaterialModule,
    Md2Module,
    // mainRoutingProviders,
    // routing
    AppRoutingModule
  ],
  providers: [
    JuridocService,
    // {
    //   provide: LOCALE_ID,
    //   useValue: 'nl-BE'
    // },
    {
      provide: LocationStrategy, 
      useClass: HashLocationStrategy
    },
  ],
  bootstrap: [RootComponent]
})
export class AppModule { }
