import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AfsluitenDossierComponent } from './afsluiten-dossier.component';

describe('AfsluitenDossierComponent', () => {
  let component: AfsluitenDossierComponent;
  let fixture: ComponentFixture<AfsluitenDossierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AfsluitenDossierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AfsluitenDossierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
