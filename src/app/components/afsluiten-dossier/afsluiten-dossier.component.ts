import { Component, OnInit, OnDestroy, HostListener, ViewChild, ElementRef } from '@angular/core';
// import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { DateAdapter, NativeDateAdapter } from '@angular/material';

//date
import * as moment from 'moment/moment';
import { DateLocale } from 'md2';

//APP
import { Reason } from '../../models/reason';
import { JuridocService } from '../../services/juridoc.service';

@Component({
  selector: 'afsluiten-dossier-component',
  templateUrl: './afsluiten-dossier.component.html',
  styleUrls: ['./afsluiten-dossier.component.css']
})
export class AfsluitenDossierComponent implements OnInit, OnDestroy { 
  private Month = {
    'long': [
      'Januari', 'Februari', 'Maart', 'April', 'Mei', 'Juni', 'Juli', 'Augustus', 'September',
      'Oktober', 'November', 'December'
    ],
    'short': ['Jan', 'Feb', 'Mrt', 'Apr', 'Mei', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'],
    'narrow': ['J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D']};

  private DaysOfWeek = {
    'long': [
      'Zondag', 'Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag', 'Zaterdag'
    ],
    'short': ['Zo', 'Ma', 'Di', 'Wo', 'Do', 'Vr', 'Za'],
    'narrow': ['Z', 'M', 'D', 'W', 'D', 'V', 'Z']};


  title = 'Afsluiten Reden App';
  reasons: Reason[] = [];
  selectedReason: Reason;
  momentDate: moment.Moment;


  constructor(
    private juridocService: JuridocService,
    private myDate: DateLocale,
    private route: ActivatedRoute,
    private router: Router,
    private titleService: Title,
    dateAdapter: DateAdapter<NativeDateAdapter>
  ) {
    
    this.myDate.months = this.Month;
    this.myDate.daysOfWeek = this.DaysOfWeek;
    this.momentDate = moment();
    this.titleService.setTitle("Afsluiten Dossier");
    dateAdapter.setLocale('nl-BE');   
  }

  isRequired = true;
  isDisabled = false;
  isOpenOnFocus = true;
  today: Date = new Date();

  private _type: string = 'date';
  set type(val: string) {
    this._type = val;
    this.dateFormat = null;
  }
  get type() {
    return this._type;
  }

  mode: string = 'auto';
  startView: string = 'month';
  selectedDate: moment.Moment;
  dateMoment: moment.Moment = moment(this.selectedDate);
  minDate: Date;
  maxDate: Date = new Date(Date.now());
  startAt: Date;
  customID: string = 'datepicker-id';
  dateFormat: string = 'd MMMM y';

  // setDate() {
  //   this.selectedDate = new Date(this.today);
  // }

  touch: boolean;
  filterOdd: boolean;
  yearView: boolean;
  dateFilter = (date: Date) => date.getMonth() % 2 == 1 && date.getDate() % 2 == 0;

  private sub: Subscription;
  private id: any;
  private parameters: string;


  ngOnInit() {
    this.sub = this.route.queryParams.subscribe(params => {
      this.parameters = params.data;

      if (this.parameters != null) {
        let vals = this.parameters.split('=');
        if (vals[0] == "dossierid") {
          this.id = vals[1]
        } else {
          window.close();
        }
      } else {
        window.close();
      }
    });
     
    this.getRejectReasons();    
  }

  getRejectReasons(): void {
    let entity = 'new_afsluitreden';
    let options = '$select=new_afsluitredenId,new_name';

    let sub = this.juridocService.retrieveMultiple(entity, options).subscribe((success) => {
      this.reasons = this.reasons.concat(success.results as Reason[]);
    });
    
    this.sub.add(sub);
  }

  submitted = false;

  onSubmit() {
    this.submitted = true;
    let new_dossier: any = {}

    new_dossier.new_AfsluitReden = { Id: this.selectedReason.new_afsluitredenId, LogicalName: "new_afsluitreden", Name: this.selectedReason.new_name };
    new_dossier.new_DatumAfsluiten = moment(this.selectedDate);

    this.juridocService.update('new_dossier', this.id, new_dossier).then(
      response => this.onDossierUpdate(), () => { window.opener.postMessage(false, "*"); window.close() }
    );
  }

  onDossierUpdate() {
    this.juridocService.setStateCode('new_dossier', this.id, '1', '-1').then(
      () => { window.opener.postMessage(true, "*"); window.close() },
      () => { window.opener.postMessage(false, "*"); window.close() }
    );
  }

  @HostListener('window:beforeunload') onbeforeunload() {
    window.opener.postMessage(false, "*");
  }

  onCloseWindow(){
    window.close();
  }
  ngOnDestroy() {
    // Clean sub to avoid memory leak
    this.sub.unsubscribe();
  }

      // @ViewChild("picker")
  // public picker: any;
  // ngAfterViewInit(){
  //   debugger;
  //   let test = this.picker.open();
  // }
}
