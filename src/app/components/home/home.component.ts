import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

import { Reason } from "../../models/reason";
import { JuridocService } from "../../services/juridoc.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    complexForm : FormGroup;

    reasonControl: FormControl;
    dateControl: FormControl;

  constructor(
    private juridocService: JuridocService) { 

  }

  ngOnInit() {
    this.createFormControls();
    this.createForm();
    this.getRejectReasons();
  }

  
  createFormControls() {
    this.reasonControl = new FormControl('', Validators.required);
    this.dateControl = new FormControl('', Validators.required);
  }

  createForm() {
    this.complexForm = new FormGroup({
        reasonControl: this.reasonControl,
        dateControl: this.dateControl
    });
  }


  submitForm(value: any, valid:any):void{
    console.log('Reactive Form Data: ')
    console.log(value);
    console.log(valid);
  }

  reasons: Reason[] = [];
  getRejectReasons(): void {
      debugger;
    let entity = 'new_afsluitreden';
    let options = '$select=new_afsluitredenId,new_name';

    this.juridocService.retrieveMultiple(entity, options).subscribe((success) => {
      this.reasons = this.reasons.concat(success.results as Reason[]);
    });
  }
}
