import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AfsluitenDossierComponent } from './components/afsluiten-dossier/afsluiten-dossier.component';
import { HomeComponent } from './components/home/home.component';
import { RootComponent } from './root.component';
// import { AppComponent } from './app.component';

const routes: Routes = [
  { path: '', redirectTo: 'root', pathMatch: 'full' },
  { path: 'root', component: RootComponent },
  { path: 'home', component: HomeComponent },
  { path: 'closefile', component: AfsluitenDossierComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
