import { TestBed, inject } from '@angular/core/testing';

import { JuridocService } from './juridoc.service';

describe('JuridocService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JuridocService]
    });
  });

  it('should be created', inject([JuridocService], (service: JuridocService) => {
    expect(service).toBeTruthy();
  }));
});
