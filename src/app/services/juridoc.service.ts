import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { HttpParams } from '@angular/common/http';



import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';

import 'rxjs/add/observable/forkjoin';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/concat';
import 'rxjs/add/observable/empty';
import 'rxjs/add/operator/map';
import "rxjs/add/operator/mergeMap";
import "rxjs/add/operator/takeUntil";
import "rxjs/add/operator/buffer";
import "rxjs/add/operator/scan";
import 'rxjs/add/operator/concat';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/expand';
import 'rxjs/add/operator/last';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';

import { Reason } from '../models/reason';

@Injectable()
export class JuridocService {

  private headers = new Headers({ 
    'Content-Type': 'application/json', 
    'Accept': 'application/json' 
  });

  constructor(private http: Http) { }

  private baseUrl(): string {
    let serviceUrl;

    try {
      serviceUrl = window.opener.parent.Xrm.Page.context.getClientUrl();
    } catch (error) {
      serviceUrl = 'http://mscrmqa.gentgrp.gent.be/JuridocDev';
    }
    return serviceUrl;
  }

  private odataUrl(): string {
    return this.baseUrl() + '/XRMServices/2011/OrganizationData.svc/';
  }

  private oserviceUrl(): string {
    return this.baseUrl() + '/XRMServices/2011/Organization.svc/web';
  }

  retrieve(entity, id, select?, expand?): Observable<any> {
    let requestOptions = new RequestOptions({ headers: this.headers });
    let url = this.odataUrl() + entity + 'Set';

    let systemQueryOptions = "";
    if (select != null || expand != null) {
      systemQueryOptions = "?";
      if (select != null) {
        var selectString = "$select=" + select;
        if (expand != null) {
          selectString = selectString + "," + expand;
        }
        systemQueryOptions = systemQueryOptions + selectString;
      }
      if (expand != null) {
        systemQueryOptions = systemQueryOptions + "&$expand=" + expand;
      }
    }

    return this.http.get(encodeURI(url + "(guid'" + id + "')" + systemQueryOptions), requestOptions).map(
      response => console.log(response)
    ).catch(this.handleError);
  }

  retrieveMultiple(entity, queryParams: string): Observable<any> {

    let requestOptions = new RequestOptions({ headers: this.headers, params: queryParams });
    let url = this.odataUrl() + entity + 'Set';

    return this.http.get(url, requestOptions).map(r => r.json().d)
      .expand((prev) => {
        if (prev.__next == null) { return Observable.empty(); }
        let decoded__next = decodeURI(prev.__next).substring((url).length);
        queryParams = decoded__next.substring(1, decoded__next.length);
        return this.http.get(url, { headers: this.headers, params: queryParams }).map(r => r.json().d);
      });
  }

  update(entity, id, object): Promise<any> {
    let url = this.odataUrl() + entity + 'Set';
    let requestOptions = new RequestOptions({  headers: this.headers });
    requestOptions.headers.append('X-HTTP-Method', 'MERGE');

    return this.http.post(url + "(guid'" + id + "')", JSON.stringify(object), requestOptions).toPromise().then(() => console.log('Updated')).catch(
      this.handleError
    );
  }

  setStateCode(entityName, entityId, stateCode, statusCode): Promise<any> {
    // create the request
    var request = "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">";
    request += "<s:Body>";
    request += "<Execute xmlns=\"http://schemas.microsoft.com/xrm/2011/Contracts/Services\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">";
    request += "<request i:type=\"b:SetStateRequest\" xmlns:a=\"http://schemas.microsoft.com/xrm/2011/Contracts\" xmlns:b=\"http://schemas.microsoft.com/crm/2011/Contracts\">";
    request += "<a:Parameters xmlns:c=\"http://schemas.datacontract.org/2004/07/System.Collections.Generic\">";
    request += "<a:KeyValuePairOfstringanyType>";
    request += "<c:key>EntityMoniker</c:key>";
    request += "<c:value i:type=\"a:EntityReference\">";
    request += "<a:Id>" + entityId + "</a:Id>";
    request += "<a:LogicalName>" + entityName + "</a:LogicalName>";
    request += "<a:Name i:nil=\"true\" />";
    request += "</c:value>";
    request += "</a:KeyValuePairOfstringanyType>";
    request += "<a:KeyValuePairOfstringanyType>";
    request += "<c:key>State</c:key>";
    request += "<c:value i:type=\"a:OptionSetValue\">";
    request += "<a:Value>" + stateCode + "</a:Value>";
    request += "</c:value>";
    request += "</a:KeyValuePairOfstringanyType>";
    request += "<a:KeyValuePairOfstringanyType>";
    request += "<c:key>Status</c:key>";
    request += "<c:value i:type=\"a:OptionSetValue\">";
    request += "<a:Value>" + statusCode + "</a:Value>";
    request += "</c:value>";
    request += "</a:KeyValuePairOfstringanyType>";
    request += "</a:Parameters>";
    request += "<a:RequestId i:nil=\"true\" />";
    request += "<a:RequestName>SetState</a:RequestName>";
    request += "</request>";
    request += "</Execute>";
    request += "</s:Body>";
    request += "</s:Envelope>";

    const headers = new Headers({
      'Content-Type': 'text/xml; charset=utf-8',
      'Accept': 'application/xml, text/xml, */*',
      'SOAPAction': 'http://schemas.microsoft.com/xrm/2011/Contracts/Services/IOrganizationService/Execute'
    });

    let requestOptions = new RequestOptions({ headers: headers });

    return this.http.post(this.oserviceUrl(), request, requestOptions)
      .toPromise()
      .then(() => console.log('set state succesful'))
      .catch(this.handleError);
  }


  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }


  // getRejectReasonsB(): Observable<Onderwerp[]> {
  //   return new Observable(observer => {
  //     let entity = 'new_Hoofdonderwerp';
  //     let filter = 'new_HoofdonderwerpId,new_name';

  //     XrmServiceToolkit.Rest.RetrieveMultiple(entity + 'Set', '$select=' + filter,
  //     function (results) {
  //       //var data = results as Onderwerp[];
  //       setTimeout(() =>  observer.next(results), 2000);

  //     },
  //     function (error) {
  //       debugger;
  //       var test = error;
  //     },
  //     function onComplete() {
  //     }, 
  //     false
  //   );
  //   }
  // ).map(response => response as Onderwerp[]);

  //   // return new Promise((resolve, reject) => {
  //   //   // let entity = 'new_afsluitreden';
  //   //   // let filter = 'new_afsluitredenId,new_name';
  //   //   let entity = 'new_Hoofdonderwerp';
  //   //   let filter = 'new_name';


  //   //   XrmServiceToolkit.Rest.RetrieveMultiple(entity + 'Set', '$select=' + filter,
  //   //   function (results) {
  //   //     debugger;
  //   //     var test = results;
  //   //     resolve(test);
  //   //   },
  //   //   function (error) {
  //   //     debugger;
  //   //     var test = error;
  //   //   },
  //   //   function onComplete() {
  //   //   }, 
  //   //   false
  //   // );
  //   // }).map(

  //   // )
  //   // .catch(this.handleError);
  // }

  // getRejectReasons(): Promise<Reason[]> {

  //   return new Promise((resolve, reject) => {
  //     let entity = 'new_afsluitreden';
  //     let filter = 'new_afsluitredenId,new_name';

  //     XrmServiceToolkit.Rest.RetrieveMultiple(entity + 'Set', '$select=' + filter,
  //       function (results) {
  //         var test = results;
  //         resolve(test);
  //       },
  //       function (error) {
  //         reject(error);
  //       },
  //       function onComplete() {
  //       },
  //       true
  //     );
  //   }).then(
  //     reasons => reasons as Reason[]
  //     )
  //     .catch(this.handleError);







  //   // return this.http.get('http://mscrmqa.gentgrp.gent.be/JuridocDev/xrmservices/2011/OrganizationData.svc/new_afsluitredenSet?$select=new_afsluitredenId,new_name',
  //   //   { headers: this.headers }
  //   // )
  //   //   .toPromise()
  //   //   .then(
  //   //   function (response) {
  //   //     let json = response.json();
  //   //     let data = json.d.results;
  //   //     let reason = data as Reason[];
  //   //     return reason;
  //   //   }
  //   //   )
  //   //   .catch(this.handleError);
  // }


  // retrieveMultiple(entity, options): Observable<any> {
  //   // const serverUrl = window.opener.parent.Xrm.Page.context.getClientUrl();
  //   // const url = serverUrl + "/XRMServices/2011/OrganizationData.svc/";


  //   let serviceUrl = 'http://mscrmqa.gentgrp.gent.be/JuridocDev/XRMServices/2011/OrganizationData.svc/';
  //   const headers = new Headers({
  //     'Content-Type': 'application/json; charset=utf-8',
  //     'Accept': 'application/json"',
  //   });

  //   let requestOptions = new RequestOptions({ headers: headers });
  //   let fullUrl = serviceUrl + entity + 'Set';
  //   let optionsString = '?' + options;


  //   return this.http.get(fullUrl + optionsString).map(response => response.json().d.results).catch(
  //     this.handleError
  //   );

  // this.http.get(fullUrl + optionsString).subscribe(response => {

  //   this.
  // });

  // return new Observable(observer => {
  //   this.http.get(fullUrl + optionsString).subscribe(response => {
  //     let data = response.json().d
  //     let results = data.results;
  //     let next = data.__next;

  //     observer.next(results);

  //     let toContinue:boolean = true;

  //     while (toContinue) {
  //       console.log('next is not null');
  //       console.log(next);
  //       optionsString = next.substring((fullUrl).length);

  //       // console.log(optionsString);
  //       this.http.get(fullUrl + optionsString).map(response => {
  //         let test =response.json().d;


  //       }).subscribe(response => {
  //         //chain request response
  //         console.log('received reply');
  //         let results = data.results;
  //         observer.next(results);
  //         next = data.__next;      
  //         console.log(next);      

  //         toContinue = false;
  //       });
  //     }
  //   });
  // });


  // return Observable.create(observer => {
  //   this.http.get(fullUrl + optionsString).map(response => {
  //     let data = response.json().d
  //     let results = data.results;
  //     let next = data.__next;

  //     while (next != null) {

  //       this.http.get(fullUrl + optionsString).map(response => {
  //         let data = response.json().d;
  //         let results = data.results;
  //         observer.next(results);
  //         next = data.__next;
  //       });
  //     }
  //     observer.onCompleted();
  //   })
  //   // Any cleanup logic might go here
  //   return () => console.log('disposed')
  // });

  // return source;
  // return Observable.fromPromise(
  //   this.http.get('').toPromise().then(response => {

  //   })
  // );

  // return new Observable(observer => {
  //   this.http.get(fullUrl + optionsString).subscribe(
  //     response => {
  //       let parsedResponse = response.json().d;
  //       console.log('response' + parsedResponse);
  //       let data = parsedResponse.results;
  //       console.log(data);
  //       let next = parsedResponse.__next;
  //       console.log('Next ' + next);  
  //       observer.next(data);
  //       if (next != null || next != 'undefined') {
  //         console.log('more stuff');  
  //         var queryOptions = next.substring((fullUrl).length);
  //         this.retrieveMultiple(entity, queryOptions)
  //       }else{
  //         console.log('completed service');
  //         observer.complete();
  //       }
  //     },
  //     (error) => console.error(error), () => console.log('complete')    
  //   )
  // }).map(response => response);


  // return this.http.get(fullUrl).map(
  //   response => response.json().d
  // );

  // .toPromise()
  // .then(response => {  })
  // .catch(this.handleError);

  // return this.http.get(serverUrl + "/" + entity + 'Set?' + options)
  // .toPromise()
  // .then(response => {  })
  // .catch(this.handleError);

  // }


  // update(entity, id, object): Promise<any> {
  //   return new Promise((resolve, reject) => {
  //     XrmServiceToolkit.Rest.Update(id, object, entity + 'Set',
  //       function () {
  //         //empty, do something though
  //         resolve();
  //       },
  //       function (error) {
  //         //errorcallback
  //         reject(error);
  //       },//async
  //       true
  //     );
  //   }).then(
  //     response => response
  //     )
  //     .catch(this.handleError);
  // }


  // setStateCode(entity, id, stateCode, statusCode): Promise<any> {
  //   debugger;
  //   return new Promise((resolve, reject) => {
  //     XrmServiceToolkit.Soap.SetState(entity, id, stateCode, statusCode,
  //       function (results) {
  //         var test = results;
  //         resolve(test);
  //       }
  //     );
  //   }).then(
  //     response => response
  //     )
  //     .catch(this.handleError);
  // }


}
