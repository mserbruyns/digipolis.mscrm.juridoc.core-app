import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'root',
  template: `<router-outlet></router-outlet>`,//routerlink
})
export class RootComponent {
  constructor() {  }
}