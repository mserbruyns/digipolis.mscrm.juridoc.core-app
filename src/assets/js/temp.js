Digipolis.Dossier.Ribbon = function () {
    var closeDossierFunction = function () {
        var dossierId = Xrm.Page.data.entity.getId();

        var customParameters = encodeURIComponent("dossierid=" + dossierId);

        var host = "http://" + document.domain;
        if (window.location.port != "") {
            host += ":" + window.location.port;
        }
        var orgname = Xrm.Page.context.getOrgUniqueName();
        var serverUrl = host + "/" + orgname;
        var url = serverUrl + "/WebResources/Digipolis_/angular/close_file.app/index.html?data=" + customParameters;

        if (window.showModalDialog) {
            window.showModalDialog(url, window, "dialogHeight:430px;dialogWidth:400px;status:no;toolbar:no;menubar:no;location:no");
            window.location.reload(true);
        }
        else {
            // Support for chrome	
            var w = window.open(url, "getspdocs", "height:430,width:400,status:no,toolbar:no,menubar:no,location:no");
            w.onload = function () {
                w.callback = function () {
                    window.location.reload(true);
                };
            };
        }
    };

    return {
        closeDossierFunction: closeDossierFunction
    };
};

window.onmessage = function (event) {
    if (event.data == true) {
        window.location.reload(true);
    }
};
